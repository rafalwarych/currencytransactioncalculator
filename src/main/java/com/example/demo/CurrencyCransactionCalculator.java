package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrencyCransactionCalculator {

	public static void main(String[] args) {
		SpringApplication.run(CurrencyCransactionCalculator.class, args);
	}

}

